package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type Satellite struct {
	Name     string       `json:name,omitempty`
	Distance float32      `json:distance,omitempty`
	Message  []string     `json:message,omitempty`
	Position *Coordinates `json:position,omitempty`
}
type Coordinates struct {
	X float32 `json:x`
	Y float32 `json:y`
}
type Source struct {
	Message  string       `json:message,omitempty`
	Position *Coordinates `json:position,omitempty`
}
type PayLoad struct {
	Satellites []*Satellite `json:satellites`
}

var Kenobi Satellite
var Skywalker Satellite
var Sato Satellite
var nuevo Satellite

func main() {

	router := mux.NewRouter()

	Kenobi = Satellite{Name: "Kenobi", Position: &Coordinates{-500, -200}}
	Skywalker = Satellite{Name: "Skylwalker", Position: &Coordinates{100, -100}}
	Sato = Satellite{Name: "Sato", Position: &Coordinates{500, 100}}

	router.HandleFunc("/", Init).Methods("GET")
	router.HandleFunc("/topsecret", TopSecret).Methods("POST")
	router.HandleFunc("/topsecret_split/{satellite_name}", TopSecretSplit).Methods("POST")
	router.HandleFunc("/topsecret_split/{satellite_name}", SatelliteInfo).Methods("GET")

	server := &http.Server{
		Addr:           ":8080",
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Println("Listening...")

	server.ListenAndServe()
}

func Init(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(Sato)
}

func GetLocation(distances ...float32) Coordinates {
	position := Coordinates{100, 233}
	return position
}

func GetMessage(messages ...[]string) (msg string) {
	return ""
}

func TopSecret(w http.ResponseWriter, r *http.Request) {

	var payload PayLoad
	_ = json.NewDecoder(r.Body).Decode(&payload)

	//	var messages []string
	//	_ = append(messages,)

	response := Source{Message: "Este es un mensaje secreto", Position: &Coordinates{X: 233.24, Y: 410.24}}
	log.Println(json.NewEncoder(w).Encode(payload))

	json.NewEncoder(w).Encode(response)
}

func TopSecretSplit(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	log.Println(params["satellite_name"])
}

func SatelliteInfo(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	log.Println(params["satellite_name"])
}
